// SPDX-FileCopyrightText: 2024 Manuel Alcaraz Zambrano <manuelalcarazzam@gmail.com>
// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

#pragma once

// Make "signals:", "slots:" visible as access specifiers
#define QT_ANNOTATE_ACCESS_SPECIFIER(a) __attribute__((annotate(#a)))

#include <QPluginLoader>

#include <KCoreAddons>
#include <KFuzzyMatcher>
#include <KProcessList>
#include <KSandbox>
#include <KAboutComponent>
#include <KAboutData>
#include <KAboutPerson>
#include <KAutoSaveFile>
#include <KBackup>
#include <KMacroExpander>
#include <KCompositeJob>
#include <KDirWatch>
#include <KFileSystemType>
#include <KFileUtils>
#include <KFormat>
#include <KUser>
#include <KJob>
#include <KJobTrackerInterface>
#include <KJobUiDelegate>
#include <KJsonUtils>
#include <KLibexec>
#include <KListOpenFilesJob>
#include <KMacroExpander>
#include <KMemoryInfo>
#include <KNetworkMounts>
#include <KOSRelease>
#include <KPluginFactory>
#include <KPluginMetaData>
#include <KProcess>
#include <KRandom>
#include <KRuntimePlatform>
#include <KSharedDataCache>
#include <KShell>
#include <KSignalHandler>
#include <KStringHandler>
#include <KTextToHTML>
#include <KUrlMimeData>
