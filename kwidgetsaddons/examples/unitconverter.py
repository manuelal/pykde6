# SPDX-FileCopyrightText: 2024 Manuel Alcaraz Zambrano <manuelalcarazzam@gmail.com>
# SPDX-License-Identifier: BSD-2-Clause

import sys

from KUnitConversion import KUnitConversion
from PySide6.QtCore import Qt
from PySide6.QtWidgets import QLabel, QApplication, QWidget, QLineEdit, QGridLayout, QComboBox

PREFIXES = ["Y", "Z", "E", "P", "T", "G", "M", "k", "h", "da", "", "d", "c", "m", "µ", "n", "p", "f", "a", "z", "y"]
LENGTH_UNITS = [x + "m" for x in PREFIXES]
MASS_UNITS = [x + "g" for x in PREFIXES]
VOLTAGE_UNITS = [x + "V" for x in PREFIXES]
CATEGORIES = {"Length": LENGTH_UNITS, "Mass": MASS_UNITS, "Voltage": VOLTAGE_UNITS}

class UnitConverter(QWidget):
    def __init__(self):
        super().__init__()

        self.text = QLabel("Hello world", alignment=Qt.AlignCenter)
        self.source_input = QLineEdit()
        self.source_input.textChanged.connect(self.update_value)
        self.converted = QLabel("")
        self.category = QComboBox()
        self.category.addItems(CATEGORIES.keys())
        self.category.currentTextChanged.connect(self.update_category)
        self.source_unit = QComboBox()
        self.source_unit.currentTextChanged.connect(self.update_value)
        self.target_unit = QComboBox()
        self.target_unit.currentTextChanged.connect(self.update_value)
        self.update_category("Length")

        self.layout = QGridLayout(self)
        self.layout.addWidget(self.category, 0, 0, 1, 2)
        self.layout.addWidget(self.source_input, 1, 0)
        self.layout.addWidget(self.source_unit, 1, 1)
        self.layout.addWidget(self.converted, 2, 0)
        self.layout.addWidget(self.target_unit, 2, 1)
    
    def update_category(self, category):
        units = CATEGORIES[category]
        self.source_unit.clear()
        self.source_unit.addItems(units)
        self.target_unit.clear()
        self.target_unit.addItems(units)

    def update_value(self):
        value = KUnitConversion.Value(self.source_input.text(), self.source_unit.currentText())
        result = value.convertTo(self.target_unit.currentText())

        self.converted.setText(str(result.number()))


if __name__ == "__main__":
    app = QApplication([])

    widget = UnitConverter()
    widget.show()

    sys.exit(app.exec())