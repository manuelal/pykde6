<!--
SPDX-FileCopyrightText: 2024 Manuel Alcaraz Zambrano <manuelalcarazzam@gmail.com>
SPDX-License-Identifier: CC0-1.0
-->

# PyKDE6

Python bindings for KDE Frameworks. It currently supports the following Frameworks:

- KCoreAddons
- KGuiAddons
- KI18n
- KNotifications
- KUnitConversion
- KWidgetsAddons
- KXmlGui

## Building

```cmd
mkdir build
cd build
cmake ..
make
```

Python wheel files for each library will be created on `build/${libraryname}/${LibraryName}/dist`. You can also use
the shared libraries (located on `build/${libraryname}/${LibraryName}/build/lib`) directly if you add the paths to
your `PYTHONPATH`.
