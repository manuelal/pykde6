// SPDX-FileCopyrightText: 2024 Manuel Alcaraz Zambrano <manuelalcarazzam@gmail.com>
// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

#pragma once

// Make "signals:", "slots:" visible as access specifiers
#define QT_ANNOTATE_ACCESS_SPECIFIER(a) __attribute__((annotate(#a)))

#include <QMetaMethod>

#include <KAboutApplicationDialog>
#include <KAboutPluginDialog>
#include <KActionCategory>
#include <KActionCollection>
#include <KBugReport>
#include <KEditToolBar>
#include <KHelpMenu>
#include <KKeySequenceWidget>
#include <KMainWindow>
#include <KShortcutsDialog>
#include <KShortcutsEditor>
#include <KShortcutWidget>
#include <KToggleToolBarAction>
#include <KToolBar>
#include <KToolTipHelper>
#include <KUndoActions>
#include <KXMLGUIBuilder>
#include <KXMLGUIClient>
#include <KXMLGUIFactory>
#include <KXmlGuiWindow>
