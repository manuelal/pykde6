// SPDX-FileCopyrightText: 2024 Manuel Alcaraz Zambrano <manuelalcarazzam@gmail.com>
// SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

#pragma once

// Make "signals:", "slots:" visible as access specifiers
#define QT_ANNOTATE_ACCESS_SPECIFIER(a) __attribute__((annotate(#a)))

#include <KCountry>
#include <KCountrySubdivision>
#include <KLazyLocalizedString>
#include <KLocalizedContext>
#include <KLocalizedString>
#include <KLocalizedTranslator>
#include <KTimeZone>
#include <KuitSetup>
